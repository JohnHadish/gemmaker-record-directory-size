# GEMmaker Record Directory Size

This Records how large a GEMmaker directory (or any directory for that matter) and logs the results

Run with the command:
```
./GEMmakerRecordDirectorySize.sh TargetDir LogName.log
```

Add a & at the end to move the job to the background

Run with the command:
```
./GEMmakerRecordDirectorySize.sh TargetDir LogName.log &
```

Has defaults settings for parameters:  
TragetDir = current dir  
LogName = GemMakerSize.log  

```
cat GemMakerSize.log_0 | xclip -selection "clipboard"
```