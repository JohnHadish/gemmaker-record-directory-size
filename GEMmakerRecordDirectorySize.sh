#!/bin/bash

#' The purpose of this script is to record the size of a GEMmaker directory.
#' The user passes in 2 arguements:
#'
#' Directory to record
#' Output log Name
#' 
#' There are defaults for each of these parameters
#'
#' Created May 28th, 2019
#' Creator JohnHadish

# Set defaults
DirectoryName="${1:-}"
LogName="${2:-GemMakerSize.log}"

#' Description: rename all older logs with a new name
#'   Test to see if there is an old log with the same name
#'   mv old log to a _1 file, make new with same name.
#'   This calls the rename function recursively
#'   TestLog.log_0 becomes TestLog_1.log
#' Created: May 28th, 2019
#' Creator: JohnHadish
rename()
{
if [ ! -f "${LogName}_${1}" ]; then
  echo Finish
else
  echo Run
  rename $[$1+1]
  mv "${LogName}_${1}" "${LogName}_$[$1+1]" 
fi
}

# Call Rename Function
rename 0

# Make the new Record File (Yes, I know it would be created below, but I like to 
# be explicit about it)
touch "${LogName}_0"

# Put Directory Name and Start Time at Beginning of File
echo $PWD$DirectoryName >> "${LogName}_0"
dt=$(date '+%d/%m/%Y %H:%M:%S');
echo "$dt" >> "${LogName}_0"

# Record Every 20 Seconds until Stop. Put time stamp as well
while true; do 
  CurrentSize=$(du $PWD/$DirectoryName -d 0 | awk '{print $1}')
  dt=$(date '+%H:%M:%S');
  echo "${CurrentSize}	${dt}" >> "${LogName}_0"
  sleep 20; done